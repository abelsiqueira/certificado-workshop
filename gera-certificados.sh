#!/bin/bash

[ $# -lt 1 ] && echo "Precisa da lista" && exit 1
lista=$(basename $1 .csv)
echo $lista

[ ! -f $lista.csv ] && echo "$lista.csv nao existe" && exit 1

# Removendo o possível CRLF
sed -i 's/\r$//g' $lista.csv

mkdir -p $lista
IFS=$'\n'

names=($(cut -d, -f1 $lista.csv))
porcs=($(cut -d, -f2 $lista.csv))
N=${#names[@]}

for i in $(seq 0 $((N-1)))
do
  name=${names[$i]}
  porc=${porcs[$i]}
  if [ "${porcs[$i]}" -lt 80 ]; then
    continue
  fi
  sed "s/FULANO/$name/" template.tex > tmp.tex
  latexmk -pdf tmp.tex
  mv tmp.pdf "$lista/$name.pdf"
done
rm -f tmp*
